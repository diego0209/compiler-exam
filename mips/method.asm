.data
	readmsg: .asciiz "Digite un numero:\n"
	num1: .byte
	num2: .byte
.text
	li $a0, 2
	jal METHOD
	
	lb $t0, 8($sp)
	sb $t0, num2
	addi $sp, $sp, 8

	li $v0, 1
	lb $a0, num2
	syscall
	
	li $v0, 10
	syscall
	
METHOD:
	move $t0, $a0
	
METHOD_REPEAT:
	li $v0, 4
	la $a0, readmsg
	syscall

	li $v0, 5
	syscall
	move $t3, $v0
	
	sub $sp, $sp, 8
	sb $t3, 8($sp)
	
	add $t1, $t1, 1
	
	blt $t1, $t0, METHOD_REPEAT

	jr $ra