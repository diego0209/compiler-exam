# make		to generate parser.x
# make lexer	to generate lexer.x
# make all	to generate parser.x and lexer.x
# make clean	to clean temp files

default: parser.x

all: parser.x lexer.x

.PHONY: src lexer

parser.x:
	$(MAKE) -C src

lexer.x:
	$(MAKE) -C lexer

clean:
	$(MAKE) -C src clean
	$(MAKE) -C lexer clean