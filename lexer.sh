# . lexer.sh subdir/file.txt
#!/bin/bash

if [ $# -gt 0 ] && [ $# -le 1 ]
then
	FILE=$1
	
	if [ -f $FILE ]; then
		./lexer/lexer.x $FILE
	else
		echo "File $FILE does not exist."
	fi
elif [ $# -gt 1 ]; then
	echo "Too many arguments."
else
	echo "No file specified."
fi
