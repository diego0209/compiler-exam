%{
	#include "syntax_tree.h"
	#include <FlexLexer.h>
	#include <iostream>
	#include <ostream>
	#include <string>
	#include <cstring>
	#include <fstream>
	#include <cstdlib>

	using namespace std;

	extern int yylineno;
	extern FILE *yyin;
	ofstream outputFile;
	int yylex();
	void yyerror(const char *s);

	Program* program;
%}
%locations
%union{
	char* oper;
	char* misc;
	char* word;
	char* method;
	char* error;
	int value;
	Instruction* instruction_;
	Var_list* var_list_;
	Method_call* method_call_;
	Assignation* assignation_;
	Print* print_;
}
%expect 0
%error-verbose

%type<instruction_> instruction
%type<var_list_> var_list
%type<method_call_> method_call
%type<assignation_> assignation
%type<print_> print

%token<oper> ASSIGN "="
%token<misc> LINEBREAK "newline"
%token<word> IDENTIFIER "identifier"
%token<method> METHOD "method" PRINT "print"
%token<value> INT "integer"
%token<error> ERROR "invalid character"

%%

init:
	| source
	;

source
	: instruction line{
		program->addInstruction($1);
	}
	| source instruction line{
		program->addInstruction($2);
	}
	| error
	;

instruction
	: assignation{
		$$ = $1;
	}
	| print{
		$$ = $1;
	}
	;

line
	: LINEBREAK
	| LINEBREAK line
	;

assignation
	: var_list ASSIGN method_call{
		$$ = new Assignation($1, $3);
	}
	| error{
		$$ = nullptr;
	}
	;

var_list
	: IDENTIFIER{
		$$ = new Var_list($1);
	}
	| var_list ',' IDENTIFIER{
		$$ = $1;
		$$->addNext($3);
	}
	| error{
		$$ = nullptr;
	}
	;

method_call
	: METHOD '(' INT ')'{
		$$ = new Method_call($3);
	}
	| error{
		$$ = nullptr;
	}
	;

print
	: PRINT IDENTIFIER{
		$$ = new Print($2);
	}
	;

%%

void yyerror(const char *s){
	fprintf(stderr, "Error at %i: %s\n", yylloc.first_line, s);
}

int main(int argc, char **argv){
	++argv, --argc;
	if(argc > 0){
		yyin = fopen( argv[0], "r" );
		program = new Program();
		yyparse();
		fclose(yyin);
		if(!yynerrs){
			if(!program->doSemanticAnalysis()){
				if(argc == 2){
					program->generateMips(argv[1]);
				} else {
					program->generateMips("output.asm");
				}
			}
		}
		delete program;
	} else {
		cout << "No file specified" << endl;
	}
	return 0;
}
