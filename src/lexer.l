%{
	#include "syntax_tree.h"
	#include "grammar.tab.h"
	#include <iostream>
	#include <string>

	using namespace std;

	int yycolumn = 1;
	#define YY_USER_ACTION {yylloc.first_line = yylineno;	\
	yylloc.first_column = yycolumn;							\
	yycolumn = yycolumn + yyleng;							\
	yylloc.last_column = yycolumn;							\
	yylloc.last_line = yylineno;}

%}

%option noyywrap
%option yylineno

ID	[a-z][a-zA-Z0-9_]*
INT	[0-9]+
BLOCKCOMMENT	\/\*((.)|\r|\n|\r\n)*\*\/
LINECOMMENT	\/\/(.)*
LINEBREAK	\r|\n|\r\n

%%

"="	{
		yylval.oper = strdup(yytext);
		return ASSIGN;
	}

"("	{
		return *yytext;
	}

")"	{
		return *yytext;
	}

","	{
		return *yytext;
	}

method	{
		yylval.method = strdup(yytext);
		return METHOD;
	}

print	{
		yylval.method = strdup(yytext);
		return PRINT;
	}

{ID}	{
		yylval.word = strdup(yytext);
		return IDENTIFIER;
	}

{INT}	{
		yylval.value = atoi(yytext);
		return INT;
	}

{LINEBREAK}	{
		yylval.misc = strdup(yytext);
		return LINEBREAK;
	}

[ \t\f]

.	{
		printf("Error at %i: lexical error, invalid character %s\n", yylineno, yytext );
		yylval.error = strdup(yytext);
		return ERROR;
	}

%%
