%{
	#include "structures.h"
	#include <FlexLexer.h>
	#include <iostream>
	#include <ostream>
	#include <string>
	#include <cstring>
	#include <fstream>
	#include <cstdlib>

	using namespace std;

	//-- Lexer prototype required by bison, aka getNextToken()
	extern int yylineno;
	extern FILE *yyin;
	ofstream outputFile;
	int yylex();
	void yyerror(const char *s);

	Program * program = new Program();
	Table* global = new Table();
	string str_cat;
	int assign_tab = 0;
	int tab = 0;
	string program_print;
	string string_value;

	void add_var_symbols(){
		if(program->var_tree) {
			Instruction* it = program->var_tree;
			Type* it_type = dynamic_cast<Type*>(it);

			while(it) {
				Variable* child = it_type->child;
				while(child){
					global->addSymbol(new Symbol(it_type->getType(), child->identifier));
					child = child->next;
				}
				it = it->next;
				it_type = dynamic_cast<Type*>(it);
			}
		}
	}

	void add_method_symbols(){
		if(program->method_tree) {
			Method* it = program->method_tree;
			while(it) {
				global->addSymbol(new Method_symbol(it->type, it->identifier, it->parameter));
				it = it->next;
			}
		}
	}

	void fill_global(){
		add_var_symbols();
		add_method_symbols();
	}

	void start_analysis() {
		Method* it = program->method_tree;

		while(it) {
			it->enter_scope(global);
			it = it->next;
		}
	}
%}
%locations

%union{
	char* operat;
	char* misc;
	char* word;
	int intval;
	float floatval;
	char* error;
	char* production;
	Var_node* var_node;
	Variable* variable;
	Instruction* instruction;
	Method_call* mth_call;
	Parameter* mth_call_param;
	While* w;
	For* f;
	Print* print;
	Method* method;
	Method_node* method_node;
	If* i;
	Concat* concat;
	Operation* operation;
	Param_dec* parDec;
	Type* t;
	ConditionConcat* cond;
	ComparisonCondition* comp;
	Cast* casting;
	Value* val;
	For_init* for_in;
}

/****************************** Tokens ******************************/

%expect 0
%error-verbose

%type<var_node> var_declaration var
%type<variable> var_list array
%type<instruction> input while if for return assignation for_block if_block  while_block control_block error_handling method_content method
%type<operation> num_operation
%type<mth_call> method_call
%type<mth_call_param>  method_call_parameter
%type<method> method_block method_declaration main_declaration
%type<print> print
%type<concat> concat concat_values
%type<t> type
%type<parDec> parameter
%type<casting> cast
%type<val> value
%type<for_in> for_init
%type<production>  sign number /*decrease*/ increase
%type <cond> condition
%type<comp> expr1
%token<operat> TASSIGN "=" TLESS "<" TLESSEQ "<=" TGREATER ">" TGREATEREQ ">=" TEQUAL"==" TNOTEQUAL "!=" TNEGATION "!" TPLUS "+" TMINUS "-" TMUL "*" TDIV "/" TMOD "%" TPLUSEQ "+=" TMINUSEQ "-=" TMULEQ "*=" TDIVEQ "/=" TMODEQ "%=" TAND "AND" TOR "OR"
%token<misc> TOPENPAREN "(" TCLOSEPAREN ")" TOPENSQRBRACK "[" TCLOSESQRBRACK "]" TCOMMA "," TCOLON ":"
%token<word> TBOOL "bool type" TCHAR "character" TCHARWORD "char type" TDOUBLE "double type" TELSE "else" TFALSE "false" TFLOAT "float type" TFOR "for" TIF "if" TINPUT "input" TINT "int type" TLONG "long type" TNULL "null" TRETURN "return" TSTRINGWORD "string type" TTRUE "true" TUNSIGNED "unsigned type" TVOID "void type" TWHILE "while" TIDENTIFIER "identifier" TSTRING "string" TLINEBREAK "\n" TEND "END" TMAIN "main" TPRINT "print"
%token<intval> TINTNUM "int number"
%token<floatval> TFLOATNUM "float number"
%token<error> ERROR

%precedence OP
%precedence INCDEC
%left TPLUS TMINUS
%left TMUL TDIV
%left TMOD
%right UMINUS

%%

/****************************** Programa ******************************/
init:
	source
;

source:
	line var_declaration method_declaration main_declaration {
		program->addVarDec(dynamic_cast<Type*>$2);
		program->addMethod($3);
		program->addMethod($4);
		program->print();
		cout << endl;
		fill_global();
		start_analysis();
		global->print();
	}
	| var_declaration method_declaration main_declaration {
		program->addVarDec(dynamic_cast<Type*>$1);
		program->addMethod($2);
		program->addMethod($3);
		program->print();
		cout << endl;
		fill_global();
		start_analysis();
		global->print();
	}
	| line method_declaration main_declaration {
		program->addMethod($2);
		program->addMethod($3);
		program->print();
		cout << endl;
		fill_global();
		start_analysis();
		global->print();
	}
	| method_declaration main_declaration {
		program->addMethod($1);
		program->addMethod($2);
		program->print();
		cout << endl;
		fill_global();
		start_analysis();
		global->print();
	}
	| var_declaration main_declaration {
		program->addVarDec(dynamic_cast<Type*>$1);
		program->addMethod($2);
		program->print();
		cout << endl;
		fill_global();
		start_analysis();
		global->print();
	}
	| main_declaration {
		program->addMethod($1);
		program->print();
		cout << endl;
		fill_global();
		start_analysis();
		global->print();
	}
	| error line
;

line:
	TLINEBREAK
	| TLINEBREAK line
	| error
;

/****************************** Operaciones numéricas ******************************/

number:
	TINTNUM { $$ = const_cast<char*>((to_string($1)).c_str()); }
	| TFLOATNUM { $$ = const_cast<char*>((to_string($1)).c_str()); }
;

value:
	num_operation { $$ = new OperationValue(ValueType::Operation, $1); }
	//| TCHAR { $$ = $1; }
	| TSTRING { $$ = new SimpleValue(ValueType::String, $1);}
	| input { $$ = new InputValue(ValueType::Input, dynamic_cast<Input*>$1);  }
;

cast:
	type TOPENPAREN value TCLOSEPAREN {
		$$ = new Cast($1->getType(), dynamic_cast<InputValue*>$3);
	}
;

assignation:
	array TASSIGN num_operation line {
		$$ = new Assignation($1, $2, $3);
	}
	| array TASSIGN TFALSE line {
		$$ = new Assignation($1, $2, new SimpleOperationTerminal(TerminalOperationType::BOOL, $3));
	}
	| array TASSIGN TTRUE line {
		$$ = new Assignation($1, $2, new SimpleOperationTerminal(TerminalOperationType::BOOL, $3));
	}
	| TIDENTIFIER TASSIGN num_operation line {
		$$ = new Assignation($1, $2, $3);
	}
	| TIDENTIFIER TASSIGN input line {
		$$ = new Assignation($1, $2, dynamic_cast<Input*>$3);
	}
;

increase:
	TIDENTIFIER TPLUS TPLUS { $$ = $1; }
;

/*decrease:
	TIDENTIFIER TMINUS TMINUS {
		str_cat = "";
		str_cat.append($1);
		str_cat.append(" ");
		str_cat.append($2);
		str_cat.append(" ");
		str_cat.append($3);
		$$ = const_cast<char*>(str_cat.c_str());
	}
;*/

num_operation:
	num_operation TPLUS num_operation {
		$$ = new SumOperation($1, $3);
	}
	| num_operation TMINUS num_operation {
		$$ = new SubOperation($1, $3);
	}
	| num_operation TMUL num_operation{
		$$ = new MultOperation($1, $3);
	}
	| num_operation TDIV num_operation{
		$$ = new DivOperation($1, $3);
	}
	| cast { $$ = new CastOperationTerminal(TerminalOperationType::Cast, $1); }
	| method_call { $$ = new MethodCallOperationTerminal(TerminalOperationType::Method_call, $1); }
	| array {
		$$ = new SimpleOperationTerminal(TerminalOperationType::Array, dynamic_cast<Array*>$1);
	}
	| TIDENTIFIER %prec OP { $$ = new SimpleOperationTerminal(TerminalOperationType::Variable, $1); }
	| number { $$ = new SimpleOperationTerminal(TerminalOperationType::Int, $1); }
;

/****************************** Operaciones lógicas ******************************/


condition:
	expr1 TAND expr1 {
		$$ = new ConditionConcat(ConditionType::AND);
		$$->addLeftCondition($1);
		$$->addRightCondition($3);
	}
	| expr1 TOR expr1 {
		$$ = new ConditionConcat(ConditionType::OR);
		$$->addLeftCondition($1);
		$$->addRightCondition($3);
	}
	//| negation TOPENPAREN condition TCLOSEPAREN
	//| TOPENPAREN condition TCLOSEPAREN
	| expr1 {
		$$ = new ConditionConcat(ConditionType::AND);
		$$->addLeftCondition($1);
	}
;

expr1:
	num_operation TLESS num_operation {
		ComparisonCondition* comparison = new ComparisonCondition(ConditionType::LESS, false);
		comparison->addLeftCondition(new TerminalCondition($1));
		comparison->addRightCondition(new TerminalCondition($3));
		$$ = comparison;
	}
	| num_operation TLESSEQ num_operation {
		ComparisonCondition* comparison = new ComparisonCondition(ConditionType::LESS, false);
		comparison->addLeftCondition(new TerminalCondition($1));
		comparison->addRightCondition(new TerminalCondition($3));
		$$ = comparison;
	}
	| num_operation TGREATER num_operation {
		ComparisonCondition* comparison = new ComparisonCondition(ConditionType::LESS, false);
		comparison->addLeftCondition(new TerminalCondition($1));
		comparison->addRightCondition(new TerminalCondition($3));
		$$ = comparison;
	}
	| num_operation TGREATEREQ num_operation {
		ComparisonCondition* comparison = new ComparisonCondition(ConditionType::LESS, false);
		comparison->addLeftCondition(new TerminalCondition($1));
		comparison->addRightCondition(new TerminalCondition($3));
		$$ = comparison;
	}
	| num_operation TEQUAL num_operation {
		ComparisonCondition* comparison = new ComparisonCondition(ConditionType::LESS, false);
		comparison->addLeftCondition(new TerminalCondition($1));
		comparison->addRightCondition(new TerminalCondition($3));
		$$ = comparison;
	}
	| num_operation TNOTEQUAL num_operation {
		ComparisonCondition* comparison = new ComparisonCondition(ConditionType::LESS, false);
		comparison->addLeftCondition(new TerminalCondition($1));
		comparison->addRightCondition(new TerminalCondition($3));
		$$ = comparison;
	}
	| num_operation TEQUAL TFALSE {
		ComparisonCondition* comparison = new ComparisonCondition(ConditionType::LESS, false);
		comparison->addLeftCondition(new TerminalCondition($1));
		comparison->addRightCondition(new TerminalCondition(new SimpleOperationTerminal(TerminalOperationType::BOOL, "false")));
		$$ = comparison;
	}
	//| value
;

/****************************** Declaración de variables ******************************/

sign:
	TUNSIGNED { $$ = $1; }
;

type:
	TINT { $$ = new Type($1); }
	| sign TINT {
		str_cat.append($1);
		str_cat.append(" ");
		str_cat.append($2);
		$$ = new Type(str_cat);
	}
	| TDOUBLE { $$ = new Type($1); }
	| sign TDOUBLE {
		str_cat.append($1);
		str_cat.append(" ");
		str_cat.append($2);
		$$ = new Type(str_cat);
	}
	| TFLOAT { $$ = new Type($1); }
	| sign TFLOAT {
		str_cat.append($1);
		str_cat.append(" ");
		str_cat.append($2);
		$$ = new Type(str_cat);
	}
	| TLONG { $$ = new Type($1); }
	| sign TLONG {
		str_cat.append($1);
		str_cat.append(" ");
		str_cat.append($2);
		$$ = new Type(str_cat);
	}
	| TBOOL { $$ = new Type($1); }
	| TCHARWORD { $$ = new Type($1); }
	| TSTRINGWORD { $$ = new Type($1); }
;

var_declaration:
	var
	| var_declaration var { program->addVarDec(dynamic_cast<Type*>($2));
	}
;

var:
	type array TASSIGN value line {
		(dynamic_cast<Type*>($1))->addVariable($2);
		$$ = $1;
	}
	| type TIDENTIFIER TASSIGN value line {
		(dynamic_cast<Type*>($1))->addVariable(new Variable($2));
		$$ = $1;
	}
	| type var_list line {
		(dynamic_cast<Type*>($1))->addVariable($2);
		$$ = $1;
	}
;

array:
	TIDENTIFIER TOPENSQRBRACK num_operation TCLOSESQRBRACK { $$ = new Array($1,$3->getOperation()); }
;

var_list:
	TIDENTIFIER TOPENSQRBRACK num_operation TCLOSESQRBRACK { $$ = new Array($1, $3->getOperation()); }
	| TIDENTIFIER { $$ = new Variable($1); }
	| var_list TCOMMA TIDENTIFIER { $$->addNext(new Variable($3)); }
;

/**************************************** Bloques ****************************************/

/****************************** Declaración de métodos ******************************/

method_declaration:
	method_block
;

method_block:
	type TIDENTIFIER TOPENPAREN parameter TCLOSEPAREN TCOLON line method return TEND line{
	$$ = new Method($1->getType(),$2);
	$$->addParam($4);
	$$->addInstruction($8);
	$$->addInstruction($9);
	}
	| type TIDENTIFIER TOPENPAREN parameter TCLOSEPAREN TCOLON line method TEND line {
	$$ = new Method($1->getType(),$2);
	$$->addParam($4);
	$$->addInstruction($8);
	}
	| TVOID TIDENTIFIER TOPENPAREN parameter TCLOSEPAREN TCOLON line method TEND line {
	$$ = new Method((new Type($1))->getType(),$2);
	$$->addParam($4);
	$$->addInstruction($8);
	}
;

parameter:
	%empty { $$ = NULL; }
	| type TIDENTIFIER { $$ = new Param_dec($1->getType(),$2); }
	| type TIDENTIFIER TCOMMA parameter {
	$$ = new Param_dec($1->getType(),$2);
	$$->next = $4; //no hay metodo de addNext para param_dec
	}
;


main_declaration:
	TINT TMAIN TOPENPAREN TCLOSEPAREN TCOLON line method return TEND line {
	$$ = new Method($1,$2);
	$$->addParam(NULL);
	$$->addInstruction($7);
	$$->addInstruction($8);
	}
	| TINT TMAIN TOPENPAREN TCLOSEPAREN TCOLON line method return TEND {
	$$ = new Method($1,$2);
	$$->addParam(NULL);
	$$->addInstruction($7);
	$$->addInstruction($8);
	}
;

return:
	TRETURN num_operation line {
	$$ = new Return($2);
	}
;

method:
	method_content {$$ = $1;}//%empty { $$ = NULL; } Puede que de problema porque el puntero lo pone en null aunque se haya
	| method_content method { $$->addNext($2); }
;

method_content:
	var { $$ = dynamic_cast<Instruction*>$1;} //LISTA :D
	| assignation { $$ = $1; }
	| if {$$ = $1;}
	| while {$$ = $1;}
 	| for {$$ = $1;}
	| print {$$ = $1;}
	| method_call {$$ = $1;}
	| error_handling {$$ = $1;}
	| input {$$ = $1;}
;


error_handling:
	TIDENTIFIER line {printf("Error at %i: syntax error, should be an assingment of variable %s.\n", @$.first_line, $1);}
	| ERROR { $$ = NULL; }
	| ERROR line {printf("Error at %i: invalid token.\n", @$.first_line);}
	| TIDENTIFIER TASSIGN line {printf("Error at %i: syntax error, missing right side of assingment.\n", @1.first_line);}
;

method_call:
	TIDENTIFIER TOPENPAREN method_call_parameter TCLOSEPAREN {
		$$ = new Method_call($1);
		$$->addParameter($3);
	}
	| TIDENTIFIER TOPENPAREN TCLOSEPAREN {
		$$ = new Method_call($1);
	}
;

method_call_parameter:
	value { $$ = new Parameter(""); }
	| value TCOMMA method_call_parameter {
		$$ = $3;
		$$->addNext(new Parameter(""));
	}
;

/****************************** If ******************************/

control_block:
	method { $$ = dynamic_cast<Instruction*>$1; }
	//| method return { $$ = dynamic_cast<Instruction*>$1; } //que se hace con el return? Pofe dijo que era mala practica que no lo acpetaramos :o
;

if:
	if_block
;

if_block:
	//TIF TOPENPAREN condition TCLOSEPAREN TCOLON line control_block TELSE TCOLON if_block TEND line { $$ = new If($3,$7,$10); }
	TIF TOPENPAREN condition TCLOSEPAREN TCOLON line control_block TELSE TCOLON line control_block TEND line {
		$$ = new If($3,$7,$11); //condition control_block control_block de else
	}
	| TIF TOPENPAREN condition TCLOSEPAREN TCOLON line control_block TEND line { $$ = new If($3,$7,NULL); }
;

/****************************** While ******************************/

while:
	while_block
;

while_block:
	TWHILE TOPENPAREN condition TCLOSEPAREN TCOLON line method TEND line { $$ = new While($3); }
;

/****************************** For ******************************/

for:
	for_block
;

for_block:
	TFOR TOPENPAREN for_init TCOMMA condition TCOMMA increase TCLOSEPAREN TCOLON line method TEND line {
		$$ = new For($3, $5, new For_iteration($7));
	}
;

for_init:
	type TIDENTIFIER TASSIGN num_operation {
		$$ = new For_init($1->getType(), dynamic_cast<OperationTerminal*>$4);
	}
;

/****************************** Print ******************************/

print:
	TPRINT concat line {
	$$ = new Print();
	$$->addConcatenation($2);
	}
;

concat:
	concat_values { $$->addNext($1); }
	| concat_values TPLUS concat { $$->addNext($1); }
;

concat_values:
	TSTRING  { $$ = new Concat($1); }
	| TIDENTIFIER { $$ = new Concat($1); }
	| method_call { $$ = new Concat($1->instructionToString()); }
;

input:
	TINPUT TOPENPAREN TSTRING TCLOSEPAREN { $$ = new Input($3);}
	| TINPUT TOPENPAREN TCLOSEPAREN { $$ = new Input(""); }
;

%%

void yyerror(const char *s){
	fprintf(stderr, "Error at %i: %s\n", yylloc.first_line, s);
}

int main(int argc, char **argv){

	++argv, --argc;	/* skip over program name */

	if ( argc > 0 )
		yyin = fopen( argv[0], "r" );
	else
		yyin = stdin;

	yyparse();

	return 0;
}
