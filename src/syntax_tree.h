#ifndef SYNTAX_TREE_H
#include <string>
#include <iostream>
#include <sstream>
#include <fstream>
#define SYNTAX_TREE_H

class Table;

enum class InstructionType{Assignation, Print};

/*
 * @class Instruction
 */
class Instruction {
public:
	Instruction* next;

	virtual ~Instruction();

	virtual void print()=0;
	virtual void generateText(std::ofstream& outputFile)=0;
	virtual short int doSemanticAnalysis(Table* table)=0;
	virtual InstructionType getInstructionType() =0;
};

/*
 * @class Var_list
 */
class Var_list{
public:
	std::string identifier;
	Var_list* next;

	Var_list();
	Var_list(std::string newIdentifier);
	~Var_list();

	int getSize();
	void addNext(std::string newIdentifier);
	void print();
};

/*
 * @class Method_call
 */
class Method_call {
public:
	int parameter;

	Method_call(int newParameter);
	~Method_call();

	void print();
	short int doSemanticAnalysis();
};

/*
 * @class Assignation
 */
class Assignation : public Instruction {
public:
	Var_list* left_side;
	Method_call* right_side;

	Assignation(Var_list* newLeft_side, Method_call* newRight_side);
	~Assignation();

	short int doSemanticAnalysis(Table* table);
	void print();
	void generateData(std::ofstream& outputFile);
	void generateText(std::ofstream& outputFile);
	InstructionType getInstructionType();
};

/*
 * @class Print
 */
class Print : public Instruction {
public:
	std::string to_print;

	Print(std::string newTo_print);
	~Print();

	short int doSemanticAnalysis(Table* table);
	void print();
	void generateText(std::ofstream& outputFile);
	InstructionType getInstructionType();
};

/*
 * @class Program
 */
class Program {
public:
	Instruction* head;
	Table* table;

	Program();
	~Program();

	void addInstruction(Instruction* newInstruction);
	void print();
	short int doSemanticAnalysis();
	void generateMips(std::string newExecutablePath);
};

#endif
