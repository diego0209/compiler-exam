#include "syntax_tree.h"
#include "symbols_table.h"

using namespace std;

/*
 * @class Instruction
 */
 Instruction::~Instruction(){}

/*
* @class Var_list
*/
Var_list::Var_list(){
	next = nullptr;
}

Var_list::Var_list(string newIdentifier){
	identifier = newIdentifier;
	next = nullptr;
}

Var_list::~Var_list(){}

int Var_list::getSize(){
	int count = 1;
	Var_list* itVar_list = next;
	while(itVar_list){
		count++;
		itVar_list = itVar_list->next;
	}
	return count;
}

void Var_list::addNext(string newIdentifier){
	if(next){
		Var_list* itVar_list = next;
		while(itVar_list->next){
			itVar_list = itVar_list->next;
		}
		itVar_list->next = new Var_list(newIdentifier);
	} else {
		next = new Var_list(newIdentifier);
	}
}

void Var_list::print(){
	cout << identifier << " ";
}

/*
 * @class Method_call
 */
Method_call::Method_call(int newParameter){
	parameter = newParameter;
}

Method_call::~Method_call(){}

void Method_call::print(){
	cout << "method ( " << parameter << " ) ";
}

short int Method_call::doSemanticAnalysis(){
	short int retval = 0;
	if(1 > parameter || parameter > 5){
		cout << "Error: invalid parameter in method call." << endl;
		retval = 1;
	}
	return retval;
}

/*
 * @class Assignation
 */
Assignation::Assignation(Var_list* newLeft_side, Method_call* newRight_side){
	left_side = newLeft_side;
	right_side = newRight_side;
}

Assignation::~Assignation(){
	Var_list* itVar_list = left_side;
	while(itVar_list){
		Var_list* temp = itVar_list;
		itVar_list = itVar_list->next;
		delete temp;
	}
	delete right_side;
}

short int Assignation::doSemanticAnalysis(Table* table){
	short int retval = 0;
	Var_list* itVar_list = left_side;
	while(itVar_list){
		if(table->searchInTable(itVar_list->identifier)!=1){
			table->addSymbol(new Symbol(itVar_list->identifier));
		}
		itVar_list = itVar_list->next;
	}
	retval = right_side->doSemanticAnalysis();
	if(left_side->getSize() != right_side->parameter){
		cout << "Error: assignation differs in numbers." << endl;
		retval = 1;
	}
	return retval;
}

void Assignation::print(){
	cout << "List size: " << left_side->getSize() << endl;
	Var_list* itVar_list = left_side;
	while(itVar_list){
		itVar_list->print();
		itVar_list = itVar_list->next;
	}
	cout << "= ";
	right_side->print();
	cout << endl;
}

void Assignation::generateData(ofstream& outputFile){
	Var_list* itVar_list = left_side;
	while(itVar_list){
		outputFile << "\t" << itVar_list->identifier << ": .byte 0" << endl;
		itVar_list = itVar_list->next;
	}
}

void Assignation::generateText(ofstream& outputFile){
	outputFile << "\tli $a0, " << right_side->parameter << endl;
	outputFile << "\tjal METHOD" << endl << endl;

	string var_ids[right_side->parameter];
	Var_list* itVar_list = left_side;
	for(int i = 0; i < right_side->parameter; i++){
		var_ids[i] = itVar_list->identifier;
		itVar_list = itVar_list->next;
	}

	for(int i = right_side->parameter; i > 0; i--){
		outputFile << "\tlb $t0, 8($sp)" << endl;
		outputFile << "\tsb $t0, " << var_ids[i-1] << endl;
		outputFile << "\taddi $sp, $sp, 8" << endl << endl;
	}
}

InstructionType Assignation::getInstructionType(){
	return InstructionType::Assignation;
}

/*
 * @class Print
 */
Print::Print(string newTo_print){
	to_print = newTo_print;
}

Print::~Print(){}

short int Print::doSemanticAnalysis(Table* table){
	short int retval = 0;
	if(table->searchInTable(to_print)!=1){
		cout << "Error: " << to_print << " was not declared." << endl;
		retval = 1;
	}
	return retval;
}

void Print::generateText(ofstream& outputFile){
	outputFile << "\tli $v0, 1" << endl;
	outputFile << "\tlb $a0, " << to_print << endl;
	outputFile << "\tsyscall" << endl;
	outputFile << "\tli $v0, 4" << endl;
	outputFile << "\tla $a0, lnbrk" << endl;
	outputFile << "\tsyscall" << endl << endl;
}

void Print::print(){
	cout << "print " << to_print << endl;
}

InstructionType Print::getInstructionType(){
	return InstructionType::Print;
}

/*
 * @class Program
 */
Program::Program(){
	head = nullptr;
	table = new Table();
}

Program::~Program(){
	Instruction* itInstr = head;
	while(itInstr){
		Instruction* temp = itInstr;
		itInstr = itInstr->next;
		delete temp;
	}
}

void Program::addInstruction(Instruction* newInstruction){
	if(head){
		Instruction* itInstr = head;
		while(itInstr->next){
			itInstr = itInstr->next;
		}
		itInstr->next = newInstruction;
	} else{
		head = newInstruction;
	}
}

void Program::print(){
	Instruction* itInstr = head;
	while(itInstr){
		itInstr->print();
		itInstr = itInstr->next;
	}
}

short int Program::doSemanticAnalysis(){
	int retval = 0;
	Instruction* itInstr = head;
	while(itInstr){
		short int semant = itInstr->doSemanticAnalysis(table);
		if(semant!=0){
			retval = semant;
		}
		itInstr = itInstr->next;
	}
	return retval;
}

void generateImplicitMethod(ofstream& outputFile){
	outputFile << "METHOD:" << endl;
	outputFile << "\tmove $t0, $a0" << endl;
	outputFile << "\tmove $t1, $zero" << endl << endl;

	outputFile << "METHOD_REPEAT:" << endl;
	outputFile << "\tli $v0, 4" << endl;
	outputFile << "\tla $a0, inputmsg" << endl;
	outputFile << "\tsyscall" << endl << endl;

	outputFile << "\tli $v0, 5" << endl;
	outputFile << "\tsyscall" << endl;
	outputFile << "\tmove $t3, $v0" << endl << endl;

	outputFile << "\tsub $sp, $sp, 8" << endl;
	outputFile << "\tsb $t3, 8($sp)" << endl << endl;

	outputFile << "\tadd $t1, $t1, 1" << endl << endl;

	outputFile << "\tblt $t1, $t0, METHOD_REPEAT" << endl;
	outputFile << "\tjr $ra" << endl;
}

void Program::generateMips(string newExecutablePath){
	ofstream outputFile;
	outputFile.open(newExecutablePath);

	outputFile << ".data" << endl;
	outputFile << "\tinputmsg: .asciiz \"Digite un número:\\n\"" << endl;
	outputFile << "\tlnbrk: .asciiz \"\\n\"" << endl;
	Instruction* itInstr = head;
	while(itInstr){
		if(itInstr->getInstructionType()==InstructionType::Assignation){
			Assignation* temp = static_cast<Assignation*>(itInstr);
			temp->generateData(outputFile);
		}
		itInstr=itInstr->next;
	}

	outputFile << "\n.text" << endl;
	itInstr = head;
	while(itInstr){
		itInstr->generateText(outputFile);
		itInstr=itInstr->next;
	}

	outputFile << "\tli $v0, 10" << endl;
	outputFile << "\tsyscall" << endl << endl;

	generateImplicitMethod(outputFile);

	outputFile.close();
}
