#include "symbols_table.h"

using namespace std;

/*
 * @class Symbol
 */
Symbol::Symbol(string newIdentifier){
	identifier = newIdentifier;
	next = nullptr;
}

Symbol::~Symbol(){}

void Symbol::print(){
	cout << identifier;
}

/*
 * @class Table
 */
Table::Table(){}

Table::~Table(){
	Symbol* itSymbol = head;
	while(itSymbol){
		Symbol* temp = itSymbol;
		itSymbol = itSymbol->next;
		delete temp;
	}
}

void Table::addSymbol(Symbol* newSymbol){
	if(head){
		Symbol* itSymbol = head;
		while(itSymbol->next){
			itSymbol = itSymbol->next;
		}
		itSymbol->next = newSymbol;
	} else {
		head = newSymbol;
	}
}

int Table::searchInTable(string identifier){
	Symbol* itSymbol = head;
	short int found = 0;
	while(itSymbol && !found){
		if(identifier.compare(itSymbol->identifier)== 0){
			found = 1;
		}
		itSymbol = itSymbol->next;
	}
	return found;
}

void Table::print(){
	cout << endl << "******************************" << endl << endl;
	cout << "\tSymbols table" << endl << endl;
	cout << "______________________________" << endl;
	cout << "------------------------------" << endl;
	Symbol* itSymbol = head;
	while(itSymbol){
		itSymbol->print();
		cout << endl;
		itSymbol = itSymbol->next;
	}
	cout << "______________________________" << endl << endl;
}
