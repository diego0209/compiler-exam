#ifndef SYMBOLS_TABLE_H
#include <string>
#include <iostream>
#define SYMBOLS_TABLE_H

/*
 * @class Symbol
 */
class Symbol{
	public:
		std::string identifier;
		Symbol* next;

		Symbol(std::string newIdentifier);
		virtual ~Symbol();

		void print();
};

/*
 * @class Table
 */
class Table{
public:
	Symbol* head;

	Table();
	~Table();

	void addSymbol(Symbol* newSymbol);
	int searchInTable(std::string identifier);
	void print();
};

#endif
