# CI-1322 Autómatas y Compiladores - Final exam
Compiler for the final exam of the course CI-1322 Autómatas y Compiladores. Universidad de Costa Rica I-2018.

## Required packages:
* build-essential
* flex
* libfl-dev
* bison