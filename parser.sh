# Script for parsing files
# . parser.sh subdir/file.txt
#!/bin/bash

if [ $# -gt 0 ] && [ $# -le 2 ]
then
	FILE=$1
	OUTPUT=$2
	
	if [ -f $FILE ]; then
		./src/parser.x $FILE $OUTPUT
	else
		echo "File $FILE does not exist."
	fi
elif [ $# -gt 2 ]; then
	echo "Too many arguments."
else
	echo "No file specified."
fi
