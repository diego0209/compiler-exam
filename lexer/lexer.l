%{

/*
	flex acddlexer.flex
	g++ -c lex.yy.c
	g++ lex.yy.o -o lexer.x
	./lexer.x example
*/

%}

%option noyywrap
%option yylineno

ID	[a-z][a-zA-Z0-9_]*
INT	[0-9]+
BLOCKCOMMENT	\/\*((.)|\r|\n|\r\n)*\*\/
LINECOMMENT	\/\/(.)*
LINEBREAK	\r|\n|\r\n

%%

"="	{
		printf("ASSIGN\n");
	}

"("	{
		printf("OPENPAREN\n");
	}

")"	{
		printf("CLOSEPAREN\n");
	}

","	{
		printf("COMMA\n");
	}

int	{
		printf("INTWORD\n");
	}

method	{
		printf("METHOD\n");
	}

print	{
		printf("PRINT\n");
	}

{ID}	{
		printf("IDENTIFIER\n");
	}

{PARAMETER}	{
		printf("PARAMETER\n");
	}

{INT}	{
		printf("INT\n");
	}

{LINEBREAK}	{
		printf("LINEBREAK\n");
	}

[ \t\f]

.	{
		printf("\nERROR at %i: %s\n\n", yylineno, yytext );
	}

%%

int main(int argc, char **argv){
	++argv, --argc;
	if ( argc > 0 )
		yyin = fopen( argv[0], "r" );
	else
		yyin = stdin;

	yylex();
}